Welcome to AGOX's documentation!
================================

**AGOX** is a Python package for global optimization of atomistic structures, leveraging state of the art machine learning 
techniques to efficiently solve complicated problems. The package has a strong focus on enabling development of 
new ideas in the area global optimization 

.. note:: 

   This documentation is under development. 

.. toctree::
   :maxdepth: 1
   :caption: Contents:


Contents
--------
.. toctree::
   :maxdepth: 2

   installation
   getting_started/getting_started
   agox_framework/agox_framework
   algorithms/algorithms
   bonus_topics/bonus_topics
   references/references
   command_line_tools/command_line

This work has been supported by funding from the Villum foundation 

.. figure:: villum_foundation.png

