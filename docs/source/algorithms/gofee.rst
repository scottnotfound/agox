GOFEE 
==============

The GOFEE algorithm is a Bayesian search algorithm first presented by Bisbo & Hammer

.. literalinclude:: scripts/gofee.py


