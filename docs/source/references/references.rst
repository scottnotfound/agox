Citation
===========

If you have used AGOX for a publication please cite

.. tabs::

    .. tab:: Text
        
        Mads-Peter V. Christiansen, Nikolaj Rønne, Bjørk Hammer, 
        `Atomistic Global Optimization X: A Python package for optimization 
        of atomistic structures <https://arxiv.org/abs/2204.01451>`_, arXiv, (2022)

    .. tab:: Bibtex

        .. code-block:: none

            @article{Christiansen2022,
            author = {Christiansen, Mads-Peter V. and Rønne, Nikolaj 
            and Hammer, Bjørk},
            journal = {arXiv},
            title = {Atomistic Global Optimization X: A Python package 
            for optimization of atomistic structures},
            volume = {},
            year = {2022}
            }

If using the GOFEE algorithm please also cite

.. tabs::

    .. tab:: Text
        
        Malthe K. Bisbo, Bjørk Hammer, `Efficient global structure optimization 
        with a machine-learned surrogate model <https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.124.086102>`_, Phys. Rev. Lett., 124, 086102, (2020)

    .. tab:: Bibtex

        .. code-block:: none

            @article{Bisbo2020,
            author = {Bisbo, Malthe K. and Hammer, Bjørk},
            journal = {Phys. Rev. Lett.},
            title = {Efficient global structure optimization 
                with a machine-learned surrogate model},
            volume = {124},
            year = {2020},
            pages = {086102}
            }

